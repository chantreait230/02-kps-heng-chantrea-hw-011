import React, { Component } from 'react';
import Calculate from './components/calculateComponent/Calculate';
import Result from './components/resultComponent/Result';
import './components/calculateComponent/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
       resultVal:[]
    }
  }

  handleResult(val){
    this.setState({
      resultVal:[...this.state.resultVal,val]
    })
  }
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-xl-3 bg2">
            <Calculate dataFromParent={
              this.handleResult.bind(this)
            }/>
          </div>
          <div className="col-xl-6 bg2">
            <div className="title">
              Calculator
            </div>
          </div>
          <div className="col-xl-3 bg2">
            <Result result={this.state.resultVal}/> 
          </div>
        </div>
      </div>
    )
  }
}
export default App

