import React from 'react'
import '../calculateComponent/style.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card,ListGroup} from 'react-bootstrap';

 function Result(props) {

    let resultNumbers = props.result;
    const listItems = resultNumbers.map((number) =>
        <ListGroup.Item as="li" key={number.id}>{number.myresult}</ListGroup.Item>
    );
    return (
        <div>
            <Card>
                <Card.Body>
                    <ListGroup as="ul">
                        <ListGroup.Item as="li" active>Result History</ListGroup.Item>
                        {listItems}
                    </ListGroup>
                </Card.Body>
            </Card>
        </div>
    )
}
export default Result

