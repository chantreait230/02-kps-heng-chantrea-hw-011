import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Form,Button,Card} from 'react-bootstrap';
import Image from '../img/1.png';


let index=1;

class Calculate extends Component {

    constructor(props) {
        super(props)
        this.state = {
             num1:'',
             num2:'',
             cal:'+'
        } 
    }

    componentDidMount(){
        this.Number1.focus();
    }

    handleChange = (e) => {
        var x=document.getElementById("number1");
        var y=document.getElementById("number2");
        this.setState({
            [e.target.name]: e.target.value
        })
        x.style.border="1px solid #ced4da";
        y.style.border="1px solid #ced4da";
    }
   
    onCalculate = (e) => {
        let result=0;
        var x=document.getElementById("number1");
        var y=document.getElementById("number2");
        if(this.state.num1===""){
            
            x.style.border="2px solid red";
            x.placeholder="Please enter number1";
            this.Number1.focus();
            return;
        }else if(isNaN(this.state.num1)){
            
            x.style.border="2px solid red";
            x.placeholder="Enter only number";
            this.Number1.focus();
            x.value="";
            return;
        }else if(this.state.num2===""){

            y.style.border="2px solid red";
            y.placeholder="Please enter number2";
            this.Number1.focus();
            return;
        }else if(isNaN(this.state.num2)){

            y.style.border="2px solid red";
            y.placeholder="Enter only number";
            this.Number1.focus();
            y.value="";
            return;
        }
        
        e.preventDefault();

        const form = {
         num1: this.state.num1,
         num2: this.state.num2,
         cal:this.state.cal
        }

        var number1=parseInt(form.num1);
        var number2=parseInt(form.num2);

        if(form.cal==="+"){
            result=number1+number2;
        }else if(form.cal==="-"){
            result=number1-number2;
        }else if(form.cal==="*"){
            result=number1*number2;
        }else if(form.cal==="/"){
            result=number1/number2;
        }else if(form.cal==="%"){
            result=number1%number2;
        }

        var myObj={
            id:index++,
            myresult:result
        }

        this.props.dataFromParent(myObj);
     }

    render() {
        return (
            <div>
                <Card>
                    <Card.Img variant="top" src={Image} />
                    <Card.Body>
                        <Form>
                            <Form.Group>
                                <Form.Control 
                                    type="text" name="num1" 
                                    value={this.state.num1} 
                                    onChange={e => this.handleChange(e)} 
                                    placeholder="Enter Number" 
                                    className="control" 
                                    id="number1"
                                    autoComplete="off"
                                    ref={(input) => { this.Number1 = input; }} 
                                    />
                            </Form.Group>
                            <Form.Group>
                                <Form.Control  
                                    type="text" name="num2" 
                                    value={this.state.num2} 
                                    placeholder="Enter Number" 
                                    onChange={e => this.handleChange(e)} 
                                    className="control" 
                                    id="number2"
                                    autoComplete="off"
                                    ref={(input) => { this.Number2 = input; }}
                                    
                                    />
                            </Form.Group>
                            <Form.Group>
                                <Form.Control as="select" name="cal" onChange={e => this.handleChange(e)} value={this.state.cal}  className="control">
                                    <option value="+">+ Plus</option>
                                    <option value="-">- Sub</option>
                                    <option value="*">* Mul</option>
                                    <option value="/">/ Cheak</option>
                                    <option value="%">%</option>
                                </Form.Control>
                            </Form.Group>
                            <Button type="button" 
                                onClick={
                                    (e)=>this.onCalculate(e)
                                }>Calculate</Button>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        )
    }
}
export default Calculate
